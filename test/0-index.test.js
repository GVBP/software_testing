var validateService = require('../main/services/validateService');

describe('Validar o tipo do Triângulo', function () {
  beforeAll(() => {
    var newObj = {};
    newObj.firstMeasure = 0;
    newObj.secondMeasure = 0;
    newObj.thirdMeasure = 0;
  });

  it('deve retornar "0", que equivale a valores inválidos', function () {
    var newObj = {};
    newObj.firstMeasure = 0;
    newObj.secondMeasure = 0;
    newObj.thirdMeasure = 0;
    expect(validateService.getTypeValueOfData(newObj)).toBe(0);
  });

  it('deve retornar "1", que equivale ao triângulo equilátero', function () {
    var newObj = {};
    newObj.firstMeasure = 3;
    newObj.secondMeasure = 3;
    newObj.thirdMeasure = 3;
    expect(validateService.getTypeValueOfData(newObj)).toBe(1);
  });

  it('deve retornar "2", que equivale ao triângulo isósceles', function () {
    var newObj = {};
    newObj.firstMeasure = 3;
    newObj.secondMeasure = 3;
    newObj.thirdMeasure = 1;
    expect(validateService.getTypeValueOfData(newObj)).toBe(2);
  });

  it('deve retornar "3", que equivale ao triângulo escaleno', function () {
    var newObj = {};
    newObj.firstMeasure = 3;
    newObj.secondMeasure = 2;
    newObj.thirdMeasure = 1;
    expect(validateService.getTypeValueOfData(newObj)).toBe(3);
  });

  it('deve retornar "true" da função que valida se é um triângulo válido', function () {
    var newObj = {};
    newObj.firstMeasure = 0;
    newObj.secondMeasure = 0;
    newObj.thirdMeasure = 0;
    expect(validateService.invalidValidation(newObj)).toBe(true);
  });

  it('deve retornar "true" da função que valida se é um triângulo equilátero', function () {
    var newObj = {};
    newObj.firstMeasure = 3;
    newObj.secondMeasure = 3;
    newObj.thirdMeasure = 3;
    expect(validateService.equilateralValidation(newObj)).toBe(true);
  });

  it('deve retornar "true" da função que valida se é um triângulo isósceles', function () {
    var newObj = {};
    newObj.firstMeasure = 3;
    newObj.secondMeasure = 3;
    newObj.thirdMeasure = 1;
    expect(validateService.isoscelesValidation(newObj)).toBe(true);
  });

  it('deve retornar "true" da função que valida se é um triângulo escaleno', function () {
    var newObj = {};
    newObj.firstMeasure = 3;
    newObj.secondMeasure = 2;
    newObj.thirdMeasure = 1;
    expect(validateService.scaleneValidation(newObj)).toBe(true);
  });
});
