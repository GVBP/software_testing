var express = require('express');
var router = express.Router();
var validateService = require('../services/validateService');

/* GET home page. */
router.get('/', function(req, res, next) {
  var data = validateService.loadFile();
  res.render('index/index', { title: 'Software Testing', data : data});
});

router.post('/', function(req, res, next) {
  var newObj = {};
  newObj.firstMeasure = req.body.firstMeasure;
  newObj.secondMeasure = req.body.secondMeasure;
  newObj.thirdMeasure = req.body.thirdMeasure;
  var typeValue = validateService.getTypeValueOfData(newObj);
  newObj.typeValue = typeValue;
  validateService.saveFile(newObj);
  res.redirect('/');
});

module.exports = router;
