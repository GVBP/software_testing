var fs = require('fs');

var filePath = "db/data.json";

var loadFile = function() {
  var file = fs.readFileSync(filePath, "UTF-8");
  var data = JSON.parse(file);
  return data;
}

var saveFile = function(data) {
  var save = JSON.stringify(data);
  fs.writeFileSync(filePath, save, 'utf8');
}

function invalidValidation(newObj) {
    var newBool = false;
    if (parseInt(newObj.firstMeasure) <= 0 || parseInt(newObj.secondMeasure) <= 0 || parseInt(newObj.thirdMeasure) <= 0) newBool = true;
    return newBool;
}

function equilateralValidation(newObj) {
    var newBool = false;
    if (newObj.firstMeasure == newObj.secondMeasure && newObj.secondMeasure == newObj.thirdMeasure) newBool = true;
    return newBool;
}

function isoscelesValidation(newObj) {
    var newBool = false;
    if (newObj.firstMeasure == newObj.secondMeasure || newObj.firstMeasure == newObj.thirdMeasure || newObj.secondMeasure == newObj.thirdMeasure) newBool = true;
    return newBool;
}

function scaleneValidation(newObj) {
    var newBool = false;
    if (newObj.firstMeasure != newObj.secondMeasure && newObj.secondMeasure != newObj.thirdMeasure) newBool = true;
    return newBool;
}

function getTypeValueOfData(newObj) {
    var typeValue = 0;
    var isTriangleType = false;
    isTriangleType = invalidValidation(newObj);
    if (!isTriangleType) {
        typeValue++;
        isTriangleType = equilateralValidation(newObj);
        if (!isTriangleType) {
            typeValue++;
            isTriangleType = isoscelesValidation(newObj);
            if (!isTriangleType) {
                typeValue++;
                isTriangleType = scaleneValidation(newObj);
            }
        }
    }
    return typeValue;
}

module.exports = {
    getTypeValueOfData: getTypeValueOfData,
    loadFile: loadFile,
    saveFile: saveFile,
    invalidValidation: invalidValidation,
    equilateralValidation: equilateralValidation,
    isoscelesValidation: isoscelesValidation,
    scaleneValidation: scaleneValidation
}